JoinMarket - installation on Windows.
====================================

This guide describes how to install JoinMarket and its dependencies (python, libsodium) on Windows.

Some or all of this may or may not work for other versions of Windows however it has been confirmed working on Windows 10 Insider Preview. It is not claimed to be in any way comprehensive. Verification of downloads are your own responsibility.

0. Install JoinMarket - go to https://github.com/chris-belcher/joinmarket and click the "Download Zip" button on the right. Unzip it into any location you choose.

1. Install Python from https://www.python.org/ftp/python/2.7.10/python-2.7.10.msi . Run the executable. Choose to install the feature **Add python.exe to Path** on local hard drive during installation; Python should then be installed in `C:\Python27`

2. Check that Python runs. Open a new command prompt as administrator by typing `cmd.exe` into the Start menu and pressing Ctrl+Shift+Enter. Type `python` and you should see something like:

    ```
    Python 2.7.10 (default....
    ....
    >>>
    ```
Exit the Python console by pressing Ctrl+C

6. Next, you need to find install the dll for libnacl. First go to https://download.libsodium.org/libsodium/releases/ and choose the file libsodium-1.0.3-msvc.zip to download. Unzip anywhere, and then copy the file libsodium.dll from the directory <libsodium-extracted-dir>\Win32\Release\v120\dynamic (do not use v140), and paste it into root joinmarket directory (the same directory where yield-generator.py lives). Then you need to address the Visual C++ 2013 runtime dependency. Do so by going to www.microsoft.com/en-us/download/details.aspx?id=40784 and clicking Download. Choose x86 even on a 64-bit system. Once the runtime is installed you should be able to go to the joinmarket directory in your command prompt and type:
    python lib\enc_wrapper.py 
and get a success message.

8. Finally it should work! You can try running the scripts `wallet-tool.py` and `yield-generator.py` and `send-payment.py`. Each has a help message to explain the syntax.

In case the Python installer won't add its paths to the environment variable PATH it could be done manually via Control Panel -> System -> Advanced Settings -> Environment Variables. Go to the  PATH  variable and add: `;C:\Python27;C:\Python27\Scripts` to the end of the string (don't forget the semi-colon).
